const anchors = document.querySelectorAll('a[href*="#"]');

for (let anchor of anchors) {
    anchor.addEventListener('click', function (e) {
        e.preventDefault();

        const blockID = anchor.getAttribute('href').substr(1);

        document.getElementById(blockID).scrollIntoView({
            behavior: 'smooth',
            block: 'start'
        });
    });
}

$("#slide").vegas({
    delay: 10000,
    overlay: '../modules/vegas/dist/overlays/07.png',
    slides: [
        {
            src: '../assets/images/trade/bukovita_img_1.jpg',
            video: {
                src: [
                    '../assets/videos/bukovita.mp4',
                ],
                mute: true,
            }
        },
        // {
        //     src: '../assets/images/land/bukovita_img_3.jpg',
        //     video: {
        //         src: [
        //             './assets/videos/1540548604163385.mp4',
        //         ],
        //         mute: true,
        //     }
        // },
        // {
        //     src: '../assets/images/land/bukovita_img_2.jpg',
        //     video: {
        //         src: [
        //             './assets/videos/1540548604163385.mp4',
        //         ],
        //         mute: true,
        //     }
        // },
        // {
        //     src: '../assets/images/land/bukovita_img_1.jpg',
        //     video: {
        //         src: [
        //             './assets/videos/1540548604163385.mp4',
        //         ],
        //         mute: true,
        //     }
        // },
    ],
});


let mobilenavLinks = document.getElementById('mobilenavLinks');
let mobileNavBtn = document.getElementById('mobileMenuBtn');
let mobileLinks = document.querySelectorAll('#mobilenavLinks > a');

let handleClickMenu = () => {
    if (mobilenavLinks.style.top === "70px") {
        mobilenavLinks.style.top = "-150px";
        mobilenavLinks.style.right = "-150px";
        mobileNavBtn.style.content = "url(../assets/icons/elements/iconfinder_Menu1_1031511.svg)";
    } else {
        mobilenavLinks.style.top = "70px";
        mobilenavLinks.style.right = "-12px";
        mobileNavBtn.style.content = "url(../assets/icons/elements/iconfinder_Close_1031533.svg)";
    }
};

let handleClickLink = () => {
    mobilenavLinks.style.top = "-150px";
    mobilenavLinks.style.right = "-150px";
    mobileNavBtn.style.content = "url(../assets/icons/elements/iconfinder_Menu1_1031511.svg)";
};

if (mobileLinks.length) {
    for (let i = 0; i < mobileLinks.length; i++) {
        const link = mobileLinks[i];
        link.addEventListener("click", handleClickLink);
    }
}

if (mobileNavBtn) {
    mobileNavBtn.addEventListener("click", handleClickMenu);
}



